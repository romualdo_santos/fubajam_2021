﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{

    [SerializeField] private FadeScreen _fadescreen;

    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("Fade in");
        if(other.tag=="Player"){
            
            _fadescreen.DoFadeIn();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
