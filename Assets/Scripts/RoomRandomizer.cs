﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomRandomizer : MonoBehaviour
{
    [SerializeField] private GameObject[] _obstaclesLayoutList;
    [SerializeField] private int _roomQuantity;
    [SerializeField] private int[] _typeFirst;
    [SerializeField] private int[] _typeQuantity;
    public static int[] typeFirst;
    public static int[] typeQuantity;
    public static GameObject[] _obstaclesLayoutNotSelected;
    public static GameObject[] obstaclesLayoutsSelected;

    // Start is called before the first frame update
    void Start()
    {
        if(typeFirst == null && typeQuantity == null)
        {
            typeFirst = _typeFirst;
            typeQuantity = _typeQuantity;
        }

        if(_obstaclesLayoutNotSelected == null)
        {
            _obstaclesLayoutNotSelected = _obstaclesLayoutList;
        }
        if(obstaclesLayoutsSelected == null){
            obstaclesLayoutsSelected = new GameObject[_roomQuantity];
        }
        
        // for (int i = 0; i < _roomQuantity; i++){
        //     obstaclesLayoutsSelected[i] = _obstaclesLayoutList[Random.Range(0, _roomQuantity)];
        // }
    }

    public static void RemoveObstacleLayoutSelected(int index){
        GameObject[] array1 = new GameObject[index];
        GameObject[] array2 = new GameObject[_obstaclesLayoutNotSelected.Length - index -1];

        for(int i = 0; i < index; i++){
            array1[i] = _obstaclesLayoutNotSelected[i];
        }

        for(int i = 0; i < _obstaclesLayoutNotSelected.Length - index -1; i++)
        {
            array2[i] = _obstaclesLayoutNotSelected[index + 1 + i];
        }

        _obstaclesLayoutNotSelected = new GameObject[array1.Length + array2.Length];

        for(int i = 0; i < array1.Length; i++)
        {
            _obstaclesLayoutNotSelected[i] = array1[i];
        }

        for(int i = 0; i < array2.Length; i++)
        {
            _obstaclesLayoutNotSelected[array1.Length + i] = array2[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
