﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndyingAmbience : MonoBehaviour
{
    public static UndyingAmbience instance = null;

    void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
