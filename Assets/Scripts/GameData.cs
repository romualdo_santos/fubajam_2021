﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Data", menuName = "Data/Game Data")]
public class GameData : ScriptableObject
{
    public bool generatorActive = false;
    public int batteryCount = 0;
    public int generatorCount = 0;
}
