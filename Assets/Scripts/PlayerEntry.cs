﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerEntry : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private GameData _gameData;
    // Start is called before the first frame update
    void Start()
    {
        if(RoomManager.currentRoom == "")
        {
            _gameData.generatorCount = 0;
            _gameData.generatorActive = false;
            _gameData.batteryCount = 0;
            Instantiate(_player, transform.position, Quaternion.identity);
            Scene currentScene = SceneManager.GetActiveScene();
            RoomManager.currentRoom = currentScene.name;
            RoomManager.entrytRoom = currentScene.name;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
