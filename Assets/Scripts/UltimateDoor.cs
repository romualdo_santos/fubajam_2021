﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltimateDoor : MonoBehaviour
{
    [SerializeField] GameData _gameData = null;
    [SerializeField] Animator _animator = null;
    [SerializeField] GameObject _collision = null;
    
    bool _inRange = false;
    AudioSource _audioSource;
    
    void Start() 
    {
        PlayerInputManager.instance.interactInputUpdate += Interacted;
        _audioSource = GetComponent<AudioSource>();
    }

    void Interacted() 
    {
        if (_inRange)
        {
            if (_gameData.generatorActive)
            {   
                _animator.SetTrigger("Open");
                _collision.SetActive(false);
                _audioSource.Play();
            }
        }
    }

    void Update() 
    {
        if (_gameData.generatorActive) 
        {
            _animator.SetTrigger("On");
        }   
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = true;
        }   
    }

    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = false;
        }   
    }
}
