﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndyingJukebox : MonoBehaviour
{
    public static UndyingJukebox instance = null;

    void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
