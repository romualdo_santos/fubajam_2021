﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class FadeScreen : MonoBehaviour
{
    public Action FinishedFading;
    
    [SerializeField] float _fadeTime = 3;
    [SerializeField] CanvasGroup _canvasGroup = null;

    void Start() 
    {
        _canvasGroup.alpha = 0;
    }

    public void DoFadeIn() 
    {
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn() 
    {
        float __timer = 0;
        while (__timer < _fadeTime) 
        {
            yield return null;
            __timer += Time.deltaTime;
            _canvasGroup.alpha = __timer / _fadeTime;
        }

        _canvasGroup.alpha = 1;
        SceneManager.LoadSceneAsync("EndScene");
    }
}
