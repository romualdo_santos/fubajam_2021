﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseMenu : MonoBehaviour
{
    [SerializeField] GameObject _mainContent = null;
    [SerializeField] GameObject _pauseContent = null;
    [SerializeField] GameObject _controlsContent = null;

    bool _isActive = false;
    
    void Start()
    {
        PlayerInputManager.instance.pauseInputUpdate += PauseInput;
        _isActive = false;
        _mainContent.SetActive(false);
        Time.timeScale = 1;
    }

    void PauseInput() 
    {
        if (_isActive)
        {
            _mainContent.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            _mainContent.SetActive(true);
            SetContent(0);
            Time.timeScale = 0;
        }
        _isActive = !_isActive;
    }

    void SetContent(int p_content) 
    {
        _pauseContent.SetActive(false);
        _controlsContent.SetActive(false);
        switch (p_content) 
        {
            case 0:
                _pauseContent.SetActive(true);
                break;

            case 1:
                _controlsContent.SetActive(true);
                break;
        }
    }

    public void ResumeButton() 
    {
        _mainContent.SetActive(false);
        Time.timeScale = 1;
        _isActive = false;
    }

    public void ControlsButton() 
    {
        SetContent(1);
    }
}
