﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBatteryCounter : MonoBehaviour
{
    [SerializeField] List<Image> _batteryIndicatorList;
    [SerializeField] Sprite _emptySprite = null;
    [SerializeField] Sprite _fullSprite = null;

    void Start() 
    {
        PlayerInventory.instance.batteryUpdate += BatteryUpdate;
        BatteryUpdate(PlayerInventory.instance.Batteries);
    }

    void BatteryUpdate(int p_count) 
    {
        for (int i = 0; i < _batteryIndicatorList.Count; i++)
        {
            _batteryIndicatorList[i].sprite = i < p_count ? _fullSprite : _emptySprite;
        }
    }
}
