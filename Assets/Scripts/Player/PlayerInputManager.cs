﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerInputManager : MonoBehaviour
{
    public static PlayerInputManager instance = null;
    public Action<Vector2> directionalInputUpdate;
    public Action actionInputUpdate;
    public Action interactInputUpdate;
    public Action pauseInputUpdate;

    Vector2 _directionalInput;

    void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogWarning("Multiple PlayerInputManager!");
        }
    }

    void Update() 
    {
        // directional
        _directionalInput = Vector2.zero;
        if (Input.GetKey(KeyCode.W)) _directionalInput += Vector2.up; 
        if (Input.GetKey(KeyCode.S)) _directionalInput += Vector2.down; 
        if (Input.GetKey(KeyCode.A)) _directionalInput += Vector2.left; 
        if (Input.GetKey(KeyCode.D)) _directionalInput += Vector2.right;
        if (directionalInputUpdate != null)
        {
            directionalInputUpdate(_directionalInput);
        }

        // action
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (actionInputUpdate != null)
            {
                actionInputUpdate();
            }
        }

        // interact
        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Return))
        {
            if (interactInputUpdate != null)
            {
                interactInputUpdate();
            }
        }

        // pause
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseInputUpdate != null)
            {
                pauseInputUpdate();
            }
        }
    }
}
