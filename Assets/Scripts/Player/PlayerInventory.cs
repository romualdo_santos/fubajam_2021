﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerInventory : MonoBehaviour
{
    public static PlayerInventory instance = null;
    public Action<int> batteryUpdate;
    public int Batteries => _batteries;

    [SerializeField] GameData _gameData = null;

    int _batteries = 0;

    void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogWarning("Multiple PlayerInventory!");
        }
    }

    void Start() 
    {
        _batteries = _gameData.batteryCount;
        if (batteryUpdate != null)
        {
            batteryUpdate(_batteries);
        }
    }

    public bool AddBattery()
    {
        _batteries++;
        if (batteryUpdate != null)
        {
            batteryUpdate(_batteries);
        }
        _gameData.batteryCount = _batteries;
        return true;
    }

    public bool RemoveBattery()
    {
        if (_batteries <= 0)
        {
            return false;
        }

        _batteries--;
        if (batteryUpdate != null)
        {
            batteryUpdate(_batteries);
        }
        _gameData.batteryCount = _batteries;
        return true;
    }
}
