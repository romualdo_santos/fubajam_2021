﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] float _velocity = 0;
    [SerializeField] [Range(1, 5)] float _runMultiplier = 1;
    [SerializeField] float _runTime = 0;
    [SerializeField] float _runCooldownTime = 0;
    [SerializeField] Slider _actionCooldownBar = null;
    [SerializeField] Animator _animator = null;
    [SerializeField] SpriteRenderer _visual = null;

    Rigidbody2D _rb2d;
    Vector2 _directionalInput = Vector2.zero;
    bool _isRunning = false;
    float _runCooldownTimer = 0;
    bool _isDead = false;
    AudioSource _audioSource;
    
    public void Die() 
    {
        Debug.Log("The player died :D");
        _audioSource.Play();
        _isDead = true;
        _animator.SetBool("Die", true);
        _animator.SetTrigger("Update");
    }
    
    void Start() 
    {
        _rb2d = GetComponent<Rigidbody2D>();
        PlayerInputManager.instance.directionalInputUpdate += DirectionalInput;
        PlayerInputManager.instance.actionInputUpdate += RunInput;
        PlayerInputManager.instance.interactInputUpdate += InteractInput;
        _audioSource = GetComponent<AudioSource>();
    }

    void Update() 
    {
        if (_isDead) return;
        if (!_isRunning)
        {
            _runCooldownTimer += Time.deltaTime;
        }
        
        _actionCooldownBar.value = _runCooldownTimer / _runCooldownTime;
        if (_runCooldownTimer / _runCooldownTime >= 1)
        {
            _actionCooldownBar.gameObject.SetActive(false);
        }
        else
        {
            _actionCooldownBar.gameObject.SetActive(true);
        }
    }

    void FixedUpdate() 
    {
        if (_isDead) return;
        _rb2d.velocity = _directionalInput.normalized * _velocity; 
        if (_isRunning) _rb2d.velocity *= _runMultiplier;
    }

    void DirectionalInput(Vector2 p_input) 
    {
        if (_isDead) return;
        if (_directionalInput != p_input)
        {
            if (p_input.magnitude > 0) 
            {
                _animator.SetBool("Walking", true);
            }
            else
            {
                _animator.SetBool("Walking", false);
            }

            if (p_input.x != 0)
            {
                _animator.SetInteger("Direction", 1);
                if (p_input.x > 0)
                {
                    _visual.flipX = false;
                }
                else
                {
                    _visual.flipX = true;
                }
            }
            else if (p_input.y != 0)
            {
                if (p_input.y > 0)
                {
                    _animator.SetInteger("Direction", 0);
                }
                else
                {
                    _animator.SetInteger("Direction", 2);
                }
            }
            
            _animator.SetTrigger("Update");
        }

        _directionalInput = p_input;
    }

    void InteractInput() 
    {
        if (_isDead) return;
        _animator.SetTrigger("Interact");
        _animator.SetTrigger("Update");
    }

    void RunInput()
    {
        if (_isDead) return;
        if (_runCooldownTimer > _runCooldownTime)
        {
            _isRunning = true;
            _runCooldownTimer = 0;
            _animator.SetBool("Running", true);
            _animator.SetFloat("RunSpeedMultiplier", _runMultiplier);
            _animator.SetTrigger("Update");
            StartCoroutine(RunTimer());
        }
    }

    IEnumerator RunTimer()
    {
        yield return new WaitForSeconds(_runTime);
        if (_isDead) yield break;
        _isRunning = false;
        _animator.SetBool("Running", false);
        _animator.SetTrigger("Update");
    }

    void OnDestroy() 
    {
        StopAllCoroutines();        
    }
}
