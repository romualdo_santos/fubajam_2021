﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    private bool _died = false;
    [SerializeField] private GameData _gameData;
    public void Death(){
        if (_died) return;
        _died = true;
        GetComponent<PlayerBehaviour>().Die();
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        _gameData.generatorCount = 0;
        _gameData.generatorActive = false;
        _gameData.batteryCount = 0;
        RoomRandomizer.typeFirst = null;
        RoomRandomizer.typeQuantity = null;
        RoomRandomizer.obstaclesLayoutsSelected = null;
        RoomRandomizer._obstaclesLayoutNotSelected = null;
        RoomManager.currentRoom = "";
        Invoke("LoadEntryRoom", 3f);
    }

    void LoadEntryRoom(){
        SceneManager.LoadScene(RoomManager.entrytRoom);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
