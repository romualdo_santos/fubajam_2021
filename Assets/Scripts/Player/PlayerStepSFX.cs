﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStepSFX : MonoBehaviour
{
    [SerializeField] AudioClip[] _stepSFX = null;

    AudioSource _audioSource;

    void Start() 
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void DoStep()
    {
        _audioSource.PlayOneShot(_stepSFX[Random.Range(0, _stepSFX.Length)]);
    }
}
