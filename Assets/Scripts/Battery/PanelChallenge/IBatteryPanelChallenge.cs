﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class IBatteryPanelChallenge : MonoBehaviour
{
    public Action<bool> challengeClosed;

    public abstract void OpenChallenge();
    public abstract void CloseChallenge();
}
