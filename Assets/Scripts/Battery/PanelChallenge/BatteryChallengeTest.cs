﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryChallengeTest : IBatteryPanelChallenge
{
    [SerializeField] GameObject _challengeUI = null;

    bool _challengeCompleted = false;
    
    public override void OpenChallenge()
    {
        _challengeUI.SetActive(true);
    }

    public override void CloseChallenge()
    {
        if (challengeClosed != null)
        {
            challengeClosed(_challengeCompleted);
        }
        _challengeUI.SetActive(false);
    }

    public void CompleteChallenge() 
    {
        _challengeCompleted = true;
    }
}
