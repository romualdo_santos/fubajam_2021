﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    [SerializeField] Animator _animatorSockets = null;
    [SerializeField] GameData _gameData = null;
    [SerializeField] AudioClip _sfx = null;
    public bool Energized => _batteries >= 3;

    int _batteries = 0;
    bool _inRange = false;
    AudioSource _audioSource;
    
    void Start() 
    {
        PlayerInputManager.instance.interactInputUpdate += Interacted;
        _batteries = _gameData.generatorCount;
        _animatorSockets.SetInteger("BatteryCount", _batteries);
        _audioSource = GetComponent<AudioSource>();
    }

    void Interacted() 
    {
        if (_inRange)
        {
            if (PlayerInventory.instance.RemoveBattery())
            {
                _batteries++;
                _animatorSockets.SetInteger("BatteryCount", _batteries);
                _gameData.generatorCount = _batteries;
                if (Energized) _gameData.generatorActive = true; 
                _audioSource.PlayOneShot(_sfx);
            }
            else
            {
                Debug.Log("player don't have battery to put in the generator");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = true;
        }   
    }

    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = false;
        }   
    }
}
