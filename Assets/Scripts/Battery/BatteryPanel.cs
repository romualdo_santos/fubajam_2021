﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPanel : MonoBehaviour
{
    // [SerializeField] IBatteryPanelChallenge _challenge = null;
    [SerializeField] Animator _animator = null;
    
    bool _inRange = false;
    bool _isChallengeCompleted = false;
    bool _isTurnedOff = false;
    AudioSource _audioSource;

    void Start() 
    {
        PlayerInputManager.instance.interactInputUpdate += Interacted;
        // _challenge.challengeClosed += ClosedChallenge;
        _audioSource = GetComponent<AudioSource>();
    }

    void Interacted() 
    {
        if (_isTurnedOff) return;
        
        if (_inRange)
        {
            if (!_isChallengeCompleted)
            {
                ClosedChallenge(true);
            }
            else
            {
                TakeBattery();
            }
        }
    }

    void OpenChallenge() 
    {
        // _challenge.OpenChallenge();
    }

    void ClosedChallenge(bool p_completed)
    {
        if (p_completed)
        {
            _isChallengeCompleted = true;
            _animator.SetBool("BatterySpawn", true);
        }
    }

    void TakeBattery() 
    {
        PlayerInventory.instance.AddBattery();
        _isTurnedOff = true;
        _animator.SetBool("BatteryTaken", true);
        _audioSource.Play();
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = true;
        }   
    }

    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = false;
        }   
    }
}
