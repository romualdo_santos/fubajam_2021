﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    bool _inRange = false;
    
    void Start() 
    {
        PlayerInputManager.instance.interactInputUpdate += Interacted;
    }

    void Interacted() 
    {
        if (_inRange)
        {
            PlayerInventory.instance.AddBattery();
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = true;
        }   
    }

    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            _inRange = false;
        }   
    }
}
