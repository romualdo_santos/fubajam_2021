﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotTrap : MonoBehaviour
{
    [Header("Parameters")]
    [Tooltip("The time the robot waits to shoot again")]
    [SerializeField] float _waitingTime = 0;
    [SerializeField] float _velocity = 0;
    
    [Header("References")]
    [SerializeField] GameObject _projectilePrefab = null;
    [SerializeField] Transform _shootPosition = null;
    [SerializeField] Animator _animator = null;

    [Header("SFX")]
    [SerializeField] AudioClip _sfxActivate = null;
    // [SerializeField] AudioClip _sfxMoving = null;
    [SerializeField] AudioClip[] _sfxShoot = null;

    int _tmp = 0;
    float _waitingTimer = 0;
    bool _goSeek = false;
    Rigidbody2D _rb2d;
    Transform _player;
    AudioSource _audioSource;

    void Start() 
    {
        GetComponentInChildren<RobotSeekRange>().playerEntered += PlayerInRange;
        _rb2d = GetComponent<Rigidbody2D>();
        _audioSource = GetComponent<AudioSource>();
    }

    void Update() 
    {
        if (_goSeek)
        {
            // Seeking
            Vector2 __diff = _player.position - transform.position;
            _rb2d.velocity = __diff.normalized * _velocity;

            // Shooting
            _waitingTimer += Time.deltaTime;
            if (_waitingTimer >= _waitingTime)
            {
                switch(_tmp % 3)
                {
                    case 0:
                        ShootStarDirection();
                        break;

                    case 1:
                        ShootCrossDirection();
                        break;

                    case 2:
                        ShootXDirection();
                        break;
                }

                _audioSource.PlayOneShot(_sfxShoot[Random.Range(0, _sfxShoot.Length)]);
                _waitingTimer = 0;
                _tmp++;
            }
        }
        else
        {
            _rb2d.velocity = Vector2.zero;
        }
    }
    
    void ShootStarDirection()
    {
        _animator.SetTrigger("Attack");
        for (int i = 0; i < 8; i++)
        {
            Instantiate(_projectilePrefab, _shootPosition.position, Quaternion.AngleAxis(45 * i, Vector3.forward));
        }
    }

    void ShootCrossDirection()
    {
        _animator.SetTrigger("Attack");
        for (int i = 0; i < 4; i++)
        {
            Instantiate(_projectilePrefab, _shootPosition.position, Quaternion.AngleAxis(90 * i, Vector3.forward));
        }
    }

    void ShootXDirection()
    {
        _animator.SetTrigger("Attack");
        for (int i = 0; i < 4; i++)
        {
            Instantiate(_projectilePrefab, _shootPosition.position, Quaternion.AngleAxis(90 * i + 45, Vector3.forward));
        }
    }

    void PlayerInRange(Transform p_player) 
    {
        if (!_goSeek)
        {
            _goSeek = true;
            _player = p_player;
            _animator.SetTrigger("Activate");
            _audioSource.Play();
            _audioSource.PlayOneShot(_sfxActivate);
        }
    }
}
