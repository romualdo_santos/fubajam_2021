﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RobotSeekRange : MonoBehaviour
{
    public Action<Transform> playerEntered;

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            if (playerEntered != null)
            {
                playerEntered(other.transform);
            }
        }    
    }
}
