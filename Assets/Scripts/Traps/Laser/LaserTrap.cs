﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTrap : MonoBehaviour
{
    [Header("Parameters")]
    [Tooltip("The time the shooter waits to shoot again")]
    [SerializeField] float _waitingTime = 0;
    [Tooltip("Initial delay to activate")]
    [SerializeField] float _lagTime = 0;
    
    [Header("References")]
    [SerializeField] GameObject _projectilePrefab = null;
    [SerializeField] Transform _shooter = null;

    [Header("SFX")]
    [SerializeField] AudioClip[] _sfxShoot = null;

    float _waitingTimer = 0;
    float _lagTimer = 0;
    AudioSource _audioSource;
    
    void Start() 
    {
        _waitingTimer = _waitingTime;
        _audioSource = GetComponent<AudioSource>();
    }

    void Update() 
    {
        if (!(_lagTimer >= _lagTime))
        {
            _lagTimer += Time.deltaTime;
            return;
        }
        
        if (_waitingTimer >= _waitingTime)
        {
            Shoot();
            _waitingTimer = 0;
        }
        else
        {
            _waitingTimer += Time.deltaTime;
        }
    }

    void Shoot() 
    {
        Instantiate(_projectilePrefab, _shooter.position, transform.rotation);
        _audioSource.PlayOneShot(_sfxShoot[Random.Range(0, _sfxShoot.Length)]);
    }
}
