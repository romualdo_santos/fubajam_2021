﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCollision : MonoBehaviour
{
    [SerializeField] bool _selfdestructOnHit = false;
    [SerializeField] GameObject _destroyEffect = null;
    
    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Player")
        {
            Debug.Log("trap killed player");
            other.GetComponent<PlayerDeath>().Death();
            if (_selfdestructOnHit)
            {
                if (_destroyEffect != null) Instantiate(_destroyEffect, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }   
    }
}
