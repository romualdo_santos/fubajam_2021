﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float _velocity = 1;
    [SerializeField] float _lifeSpam = 5;

    Rigidbody2D _rb2d;
    float _timer = 0;
    
    void Start() 
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _rb2d.velocity = transform.rotation * Vector2.up * _velocity;
    }

    void Update() 
    {
        _timer += Time.deltaTime;
        if (_timer >= _lifeSpam)
        {
            Destroy(gameObject);
        }
    }
}
