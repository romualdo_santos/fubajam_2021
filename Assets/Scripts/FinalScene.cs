﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalScene : MonoBehaviour
{
    [SerializeField] CanvasGroup _canvasGroup = null;
    [SerializeField] float _fadeTime = 0;
    [SerializeField] float _readTime = 0;

    void Start() 
    {
        _canvasGroup.alpha = 1;
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut() 
    {
        Debug.Log("Gfade out");
        float __timer = 0;
        while (__timer < _fadeTime) 
        {
            yield return null;
            __timer += Time.deltaTime;
            _canvasGroup.alpha = 1 - __timer / _fadeTime;
        }

        _canvasGroup.alpha = 0;
        StartCoroutine(TimerToRead());
    }

    IEnumerator TimerToRead()
    {
        yield return new WaitForSeconds(_readTime);
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn() 
    {
        float __timer = 0;
        while (__timer < _fadeTime) 
        {
            yield return null;
            __timer += Time.deltaTime;
            _canvasGroup.alpha = __timer / _fadeTime;
        }

        _canvasGroup.alpha = 1;
        SceneManager.LoadSceneAsync("MenuScene");
    }
}
