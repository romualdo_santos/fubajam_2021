﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleLayoutContainer : MonoBehaviour
{
    [SerializeField] private int _roomIndex;
    [SerializeField] private int _roomType;    
    [SerializeField] private GameObject[] _doors;
    
    // Start is called before the first frame update
    void Start()
    {
        if(RoomRandomizer.obstaclesLayoutsSelected[_roomIndex] == null)
        {
            int number;
            if(RoomRandomizer.typeQuantity[_roomType] > 1){
                number = Random.Range(RoomRandomizer.typeFirst[_roomType], RoomRandomizer.typeFirst[_roomType] + RoomRandomizer.typeQuantity[_roomType]);
                
            }

            else{
                number = RoomRandomizer.typeFirst[_roomType];
            }

            RoomRandomizer.obstaclesLayoutsSelected[_roomIndex] = RoomRandomizer._obstaclesLayoutNotSelected[number];
            RoomRandomizer.RemoveObstacleLayoutSelected(number);
            RoomRandomizer.typeQuantity[_roomType]--;

            if(_roomType != RoomRandomizer.typeQuantity.Length - 1){
                for(int i = _roomType + 1; i < RoomRandomizer.typeFirst.Length; i++){
                    RoomRandomizer.typeFirst[i]--;
                }
            }
            
        }

        Instantiate(RoomRandomizer.obstaclesLayoutsSelected[_roomIndex], transform.position, Quaternion.identity);

        _doors = GameObject.FindGameObjectsWithTag("Door");

        for (int i = 0; i < _doors.Length; i++){
            _doors[i].SetActive(true);
        }

        // for (int i = 0; i < _obstaclesLayoutList.Count; i++){
        //     obstaclesLayoutsSelected[i] = _obstaclesLayoutList[Random.Range(0, _roomQuantity)];
        // }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
