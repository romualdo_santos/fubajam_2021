﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _subMenu;
    [SerializeField] private GameObject _mainMenu;
    // Start is called before the first frame update
    public void Play(){
        RoomManager.currentRoom = "";
        SceneManager.LoadScene("EntryRoom");
    }

    public void OpenSubMenu(){
        _mainMenu.SetActive(false);
        _subMenu.SetActive(true);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
