﻿using UnityEngine;
using System.Collections;
 
public class DestroyAnimationEnd : MonoBehaviour 
{
    [SerializeField] float _delay = 0f;
    [SerializeField] Animator _animator = null;

    void Start () 
    {
        Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length + _delay);
    }
}