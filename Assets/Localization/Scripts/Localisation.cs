﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Localisation
{
    public enum Languange{
        PortuguesBr,
        English
    }

    public static Languange languange = Languange.English;

    private static Dictionary<string, string> localisedBR;
    private static Dictionary<string, string> localisedEN;

    public static bool isInit = false;

    public static CSVLoader csvLoader;

    public static void Init(){
        csvLoader = new CSVLoader();
        csvLoader.LoadCSV();

        UpdateDictionaries();

        isInit = true;
    }

    public static void UpdateDictionaries(){
        localisedBR = csvLoader.GetDictionaryValues("br");
        localisedEN = csvLoader.GetDictionaryValues("en");
    }

    public static Dictionary<string, string> GetDictionaryForEditor(){
        if(!isInit){ Init(); }
        return localisedEN;
    }

    public static string GetLocalisedValue(string key){
        if(!isInit){ Init(); }

        string value = key;

        switch (languange)
        {
            case Languange.PortuguesBr:
                localisedBR.TryGetValue(key, out value);
                break;
            case Languange.English:
                localisedEN.TryGetValue(key, out value);
                break;
        }

        return value;
    }

    public static void Add(string key, string value){
        if(value.Contains("\"")){
            value.Replace('"', '\"');
        }

        if(csvLoader == null){
            csvLoader = new CSVLoader();
        }

        csvLoader.LoadCSV();
        csvLoader.Add(key, value);
        csvLoader.LoadCSV();

        UpdateDictionaries();
    }

    public static void Replace(string key, string value){
        if(value.Contains("\"")){
            value.Replace('"', '\"');
        }

        if(csvLoader == null){
            csvLoader = new CSVLoader();
        }

        csvLoader.LoadCSV();
        csvLoader.Edit(key, value);
        csvLoader.LoadCSV();

        UpdateDictionaries();
    }

    public static void Remove(string key){
        if(csvLoader == null){
            csvLoader = new CSVLoader();
        }

        csvLoader.LoadCSV();
        csvLoader.Remove(key);
        csvLoader.LoadCSV();

        UpdateDictionaries();
    }
}
