﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextLocalisedUI : MonoBehaviour
{

    TextMeshProUGUI textField;

    public LocalisedString localisedString;

    // Start is called before the first frame update
    void OnEnable()
    {
        textField = GetComponent<TextMeshProUGUI>();               
        UpdateText();
        if (GetComponent<DialogueSystem>() != null) 
        {
            GetComponent<DialogueSystem>().InitDialogue();
        }
    }

    public void UpdateText()
    {
        textField.text = localisedString.value;
    }
}
