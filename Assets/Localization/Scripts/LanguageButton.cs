﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageButton : MonoBehaviour
{
    //[SerializeField] private Localisation.Languange _languange;    
    private int _languangeCount;
    private int _languangeIndex;    
    // Start is called before the first frame update
    void Start()
    {
      //  _languange = Localisation.languange;        
        _languangeCount = System.Enum.GetNames(typeof(Localisation.Languange)).Length;
        _languangeIndex = (int)Localisation.languange;
    }

    // Update is called once per frame
    public void selectLanguage(){
        if(_languangeIndex < _languangeCount - 1)
        {
            _languangeIndex++;
        }else
        {
            _languangeIndex = 0;
        }
        PlayerPrefs.SetInt("language", _languangeIndex);
        //_languange = (Localisation.Languange)_languangeIndex;
        Localisation.languange = (Localisation.Languange)_languangeIndex;


        var texts = FindObjectsOfType<TextLocalisedUI>();

        for(int i = 0; i < texts.Length; i++)
        {
            texts[i].GetComponent<TextLocalisedUI>().UpdateText();
        }
    }
}
